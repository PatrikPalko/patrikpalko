import React from "react";
import { Switch, Route } from "react-router-dom";
import App from "./App";
import styled from "styled-components";
const Navigation = styled.div;

function Container() {
    return (
        <Navigation>
            <Switch>
                <Route exact path="/" component={App} />
            </Switch>
        </Navigation>
    ) ;
}

export default Container;