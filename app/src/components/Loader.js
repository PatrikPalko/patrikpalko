import React from "react";
import InlineSVG from 'svg-inline-react';
const logo = `<svg class="logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 241.63 362.32"><polygon class="third" points="241.63 71.47 135.53 0 0 0 241.63 162.79 241.63 71.47" style="fill-rule:evenodd"/><polygon class="first" points="19.93 149.61 94.83 99.61 94.83 362.32 19.93 312.02 19.93 149.61" style="fill-rule:evenodd"/><polygon class="second" points="241.63 162.79 19.93 312.02 19.93 220.83 241.63 71.47 241.63 162.79" style="fill-rule:evenodd"/></svg>`;

const Loader = () => {
    return (
        <div className="tpl__loader">
            <div className="tpl">
                <div className="container-medium">
                    <div className="row">
                        <div className="grid-xs-24">
                            <div className="content">
                                <div className="logoWrapper">
                                    <InlineSVG src={logo} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default Loader;